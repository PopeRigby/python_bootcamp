print('What is your favorite animal?')
animal = input()

if animal: # checks whether 'animal' is a non-empty string, and is therefore truthy
    print(animal + ' is my favorite too!')
else:
	print('YOU DIDNT SAY ANYTHING!')