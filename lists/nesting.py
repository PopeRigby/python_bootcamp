coordinates = [[10.423, 9.132], [37.212, -14.092], [21.367, 32.572]]
for location in coordinates:
	for coordinate in location:
		print(coordinate)
# for every location list in the coordinates var, and for every coordinate in that location list, print the coordinate

answer = [[number for number in range(0,3)] for item in range(0,3)]
# create a number for every number in a range 0-2, for every item in a range 0-2 (kind of confusing)
# creates: [[0, 1, 2], [0, 1, 2], [0, 1, 2]]