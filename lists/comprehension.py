answer = [name[0] for name in ["Elie", "Tim", "Matt"]]
# store the first letter in the 'answer' variable for every name in the list

answer2 = [number for number in [1,2,3,4,5,6] if number % 2 == 0]
# store the number in 'answer2' for every number in the list, if it's even

answer3 = [letter for letter in "amazing" if letter not in "aeiou"]
# store the letter in 'answer3' for every letter in "amazing", if the letter isn't a vowel